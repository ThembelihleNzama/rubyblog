class UsersController < ApplicationController
    before_action :check_authorization, only: [:edit, :update]
    def new 
        @user = User.new
    end

    def edit
        @user =User.find(current_user.id)
        current_user
    end
    def update
        # render plain:params[:user].inspect
        @user =User.find(current_user.id)

        if @user.update(params[:user].permit(:name, :surname, :avatar))
            redirect_to @user
        else
            flash[:alert] = "Something went wrong. Please try again"
            render :edit
        end
    end

    def create 
        # render plain:params[:user].inspect
        @user = User.new(user_params) 
        
        if(@user.save)
          session[:user_id] = @user.id 
          redirect_to '/' 
        else
            redirect_to 'new'
        end
    end

    private def user_params 
        params.require(:user).permit(:name, :surname, :email, :password, :avatar)
    end

    private def check_authorization
        unless current_user.id == params[:id].to_i
            redirect_to "/"
        end
    end
end

