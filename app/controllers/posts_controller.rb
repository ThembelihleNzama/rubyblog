class PostsController < ApplicationController
  require 'will_paginate/array'
  def index
    @post = Post.paginate(:page => params[:page],:per_page => 2)
  end
  
  def new 
    @post = Post.new
  end

  def show
    @post = Post.find(params[:id])
  end

  def edit
    @post =Post.find(params[:id])
  end

  def update 
    @post =Post.find(params[:id])

    if(@post.update(post_params))
        redirect_to @post
      else
        render 'edit'
    end
  end

  def destroy
    @post =Post.find(params[:id])

    @post.destroy
    redirect_to posts_path
  end
  
  def create
    #render plain:params[:post].inspect
    
    @post = Post.create(post_params.merge(user_id: current_user.id))

    if(@post.save)
          redirect_to @post
    else
          render 'new'
    end
  end

  private def post_params
      params.require(:post).permit(:title, :body)
      # authorize @post
  end
  end
