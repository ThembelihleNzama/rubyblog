class User < ApplicationRecord
    has_secure_password

    has_many :posts, dependent: :destroy
    # has_many :comments through :posts

     mount_uploader :avatar, AvatarUploader

    # before_save :update_avatars_attributes

    # private
    #     def update_avatars_attributes
    #         if avatars.present? && avatars_changed?
    #             self.avatars= avatars.file.original_filename
    #             # self.avatars_file_size = avatars_file_size
    #     end
    # end
end
