Rails.application.routes.draw do
  root 'posts#index', as: 'home'

  resources :posts do
   resources :comments
  end

  resources :users ,only: [:new , :create, :show, :edit, :update]

get 'new'=> 'users#new'
get 'show'=> 'users#show'
# patch '/users/:id/edit' => 'users#update'
get '/login' => 'sessions#new'
post 'login' => 'sessions#create'
delete 'logout' => 'sessions#destroy'
end
